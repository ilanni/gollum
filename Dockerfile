FROM registry.docker-cn.com/library/ruby:2.4.1-alpine
MAINTAINER ilanni <ilanni@ilanni.com>
RUN set -xe \
&& echo "https://mirrors.aliyun.com/alpine/v3.4/main" > /etc/apk/repositories \
&& echo "https://mirrors.aliyun.com/alpine/v3.4/community" >> /etc/apk/repositories \
&& apk --update add alpine-sdk icu-dev git rsync openssh \
&& gem install gollum github-markdown \
&& rm -rf /var/cache/apk/* \
&& mkdir -p /gollum/ \
&& git init /gollum/
# Expose default gollum port 4567
EXPOSE 4567

ENTRYPOINT ["/usr/local/bundle/bin/gollum", "/gollum/"]