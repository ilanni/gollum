可以下载该仓库，然后直接执行docker bild即可，例子如下：
```
docker build -t registry.cn-hangzhou.aliyuncs.com/ilanni/gollum ./
```

```
docker push registry.cn-hangzhou.aliyuncs.com/ilanni/gollum
```

docker push完毕后。我们可以使用如下命令运行该镜像：
```
docker run -d -it --name gollum -P -p 8080:4567 -v /gollum/qjd_wiki:/gollum/ --privileged=true ilanni/gollum --allow-uploads --live-preview
``` 
注意：上述运行命令中 --privileged=true 特权模式一定要加上否则，会报无法创建目录的权限错误。
如果要禁用web页面的编辑功能的话，需要加入--no-edit选项，如下：
```
docker run -d -it --name gollum -P -p 8080:4567 -v /gollum/qjd_wiki:/gollum/ --privileged=true ilanni/gollum --allow-uploads --live-preview --no-edit
```